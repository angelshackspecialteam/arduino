#ifndef API_MOBIL_H
#define API_MOBIL_H

#include "Arduino.h"
#include "SoftwareSerial.h"

class ApiMobil {
	public:
		ApiMobil(int BT_TX, int BT_RX, int baud_rate);
		void send(String data);
    char receive();
	private:
		SoftwareSerial BT;
};

#endif
