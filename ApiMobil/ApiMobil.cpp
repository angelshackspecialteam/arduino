#include "Arduino.h"
#include "ApiMobil.hpp"

ApiMobil::ApiMobil(int BT_TX, int BT_RX, int baud_rate) : BT(BT_TX, BT_RX) {
	BT.begin(baud_rate);
}

void ApiMobil::send(String data) {
	if (BT.available()) BT.print(data);
}

char ApiMobil::receive() {
  if (BT.available()) {
    return BT.read();
  }
  return ' ';
}

