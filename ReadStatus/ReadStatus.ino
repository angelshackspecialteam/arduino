//Joystick pins
#define JS_X A0
#define JS_Y A1
#define JS_B 12

//Button pins
#define B_1 3
#define B_2 4
#define B_3 5
#define B_4 6

//Motor pin
#define M_1 7

//Define state variables
String JS_X_val, JS_Y_val, buttons;
bool JS_B_val, B_1_val, B_2_val, B_3_val, B_4_val, M_1_val = false;

String getButtonsHexa(){
  JS_B_val = digitalRead(JS_B);
  B_1_val = digitalRead(B_1);
  B_2_val = digitalRead(B_2);
  B_3_val = digitalRead(B_3);
  B_4_val = digitalRead(B_4);
  JS_B_val = !JS_B_val;
  int data = JS_B_val, tmp;
  data = data << 1;
  tmp = B_1_val;
  data = data or tmp;
  data = data << 1;
  tmp = B_2_val;
  data = data or tmp;
  data = data << 1;
  tmp = B_3_val;
  data = data or tmp;
  data = data << 1;
  tmp = B_3_val;
  data = data or tmp;
  data = data << 1;
  tmp = B_4_val;
  data = data or tmp;
  return String(data, HEX);
}

//Initial configuration
void setup()
{
  Serial.begin(9600);
  pinMode(JS_X, INPUT);
  pinMode(JS_Y, INPUT);
  pinMode(JS_B, INPUT);
  pinMode(B_1, INPUT);
  pinMode(B_2, INPUT);
  pinMode(B_3, INPUT);
  pinMode(B_4, INPUT);
  pinMode(M_1, OUTPUT);
  delay(500);
}

//Programming logic
void loop()
{
  JS_X_val = String(analogRead(JS_X), HEX);
  JS_Y_val = String(analogRead(JS_Y), HEX);
  buttons = getButtonsHexa();
  //Simple protocol
  Serial.println(JS_X_val + JS_Y_val + buttons);
  //Motor
  digitalWrite(M_1, M_1_val);
  delay(100);
  
}
