#include <GY_85.h>
#include <Wire.h>

GY_85 GY85;

void setup()
{
  Wire.begin();
  delay(10);
  Serial.begin(9600);
  delay(10);
  GY85.init();
  delay(10);
}

void loop()
{
  int ax = GY85.accelerometer_x(GY85.readFromAccelerometer());
  int ay = GY85.accelerometer_y(GY85.readFromAccelerometer());
  int az = GY85.accelerometer_z(GY85.readFromAccelerometer());

  int cx = GY85.compass_x(GY85.readFromCompass());
  int cy = GY85.compass_y(GY85.readFromCompass());
  int cz = GY85.compass_z(GY85.readFromCompass());
  
  int gx = GY85.gyro_x(GY85.readGyro());
  int gy = GY85.gyro_y(GY85.readGyro());
  int gz = GY85.gyro_z(GY85.readGyro());
  
  delay(100);
}
